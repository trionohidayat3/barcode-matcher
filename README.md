# Barcode Matcher

Barcode Matcher adalah aplikasi Android yang dibangun menggunakan Android Studio dengan bahasa pemrograman Java. Aplikasi ini memungkinkan pengguna untuk memindai dan membandingkan kode barcode menggunakan perangkat scanner.

## Fitur

- Memindai kode barcode menggunakan perangkat scanner
- Membandingkan kode barcode yang dipindai dengan daftar kode barcode yang ada
- Menampilkan hasil perbandingan kode barcode
- Menggunakan algoritma pencocokan barcode yang efisien
- Export hasil perbandingan ke file xls

## Persyaratan Sistem

- Scanner Barcode dengan sistem operasi Android
- Android 5.0 (API level 21) atau yang lebih baru

## Instalasi

1. Clone repositori ini:

```
git clone https://gitlab.com/trionohidayat3/barcode-matcher.git
```
2. Buka Android Studio.
3. Pilih "Open an existing Android Studio project".
4. Arahkan ke direktori tempat Anda mengkloning repositori.
5. Pilih direktori "barcode-matcher" dan klik "OK".
6. Tunggu hingga Android Studio mengimpor proyek dan mempersiapkannya.
7. Setelah proyek terbuka, klik tombol "Run" untuk menjalankan aplikasi di emulator atau perangkat fisik Anda.

## Kontribusi

Kami senang menerima kontribusi dari komunitas! Jika Anda ingin berkontribusi pada proyek ini, silakan ikuti langkah-langkah berikut:

1. Fork repositori ini.
2. Buat branch baru dengan fitur yang ingin Anda tambahkan atau perbaiki.
3. Lakukan perubahan yang diperlukan.
4. Lakukan commit dan push perubahan ke branch Anda.
5. Buat "Pull Request" dari branch Anda ke branch "main" di repositori ini.
6. Tim pengembang akan memeriksa kontribusi Anda dan memberikan umpan balik.

## Lisensi

Proyek ini dilisensikan di bawah lisensi MIT. Silakan lihat file [LICENSE](LICENSE) untuk informasi lebih lanjut.

---

Terima kasih telah menggunakan Barcode Matcher! Jika Anda memiliki pertanyaan atau masalah, jangan ragu untuk membuat "Issue" di repositori ini.